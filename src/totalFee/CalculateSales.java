package totalFee;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;




public class CalculateSales {
	public static void main(String[] arge) {

		Map<String, String> map = new HashMap<>();
		Map<String,Long> map2 = new HashMap<String,Long>();



		try{//支店定義ファイル

			String[] fruit = new String[100];
			String line ;
			//データを入力する先のファイル定義---------------------------------------
			File cdirectory = new File(arge[0]);
			String filelist[] = cdirectory.list();
			File file = new File(arge[0],"branch.lst");
				if(!file.exists()){
					System.out.println("支店定義ファイルが存在しません。");
					return;
				}else {
					FileReader fr = new FileReader(file);
					BufferedReader br = new BufferedReader(fr);
			//------------------------------------------------------------------------


			//指定したTXTファイルの中身を一行ずつ読み込む-----------------------------
					while((line = br.readLine()) != null){
					fruit = line.split(",", 0);
						if(fruit[0].matches("^[0-9]{3}")==true && fruit.length == 2){
							map.put(fruit[0],fruit[1]);
							map2.put(fruit[0],0L);
						}else{
							System.out.println("支店定義ファイルのフォーマットが不正です。");
							return;
						}
					}
			}
			//------------------------------------------------------------------------
		}catch (IOException e) {
				System.out.println(e);
		} finally {
			System.out.println("支店定義ファイルを終了しました。");
		}


		try{//売上ファイル
			String line ;
			List<String> list1 = new ArrayList<String>();
			File cdirectory = new File(arge[0]);
			String filelist[] = cdirectory.list();
	    	for (int i = 0 ; i < filelist.length ; i++){
	    		if(filelist[i].matches("^[0-9]{8}.rcd$")==true){
	    			list1.add(filelist[i]);
	    			}
	    		}
			for(int i= 0; i<list1.size()-1; i++){
				 String result = list1.get(i).substring(0,8);
				 String result1 = list1.get(i+1).substring(0,8);
				 int v = Integer.parseInt(result);
				 int v1 = Integer.parseInt(result1);
				 if(v1 - v == 1){
				 }else{
					 System.out.println("売上ファイル名が連番になっていません");
					 return;
				 }
			for(int i1= 0; i1<list1.size(); i1++){
				 File cdirectory1 = new File(arge[0],list1.get(i1));
				 FileReader fr = new FileReader(cdirectory1);
				 BufferedReader br = new BufferedReader(fr);
				 List<String> list = new ArrayList<String>();
				 while((line = br.readLine()) != null){
				 	list.add(line);
				 }
				 long a = Long.parseLong(list.get(1));
				 	if(!map.containsKey(list.get(0))){
				 		 System.out.println(list.get(0)+"の支店コードが不正です");
				 			return;
				 	}else{
				 	if( map2.get(list.get(0)) + a <1000000000){
				 		map2.put(list.get(0), map2.get(list.get(0)) + a);
				 	}else{
				 		 System.out.println("合計金額が10桁を超えました。");
				 			return;
				 	}
				 }
			}
			}
		}catch (IOException e){
			System.out.println(e);
		}finally {
			System.out.println("売上ファイルを終了しました。");
		}

		try{//集計ファイル

			//データを出力する先のファイル定義----------

			File file = new File(arge[0],"branch.out");
			FileWriter fw = new FileWriter(file);
			BufferedWriter bw = new BufferedWriter(fw);

			//-------------------------------------------

				for(Map.Entry<String, String> storeStatus : map.entrySet()) {
				bw.write(storeStatus.getKey() + " : " + storeStatus.getValue()+" : "+"合計金額"+map2.get(storeStatus.getKey())+"\r\n");
				 }

		}catch (IOException e) {
			System.out.println(e);
		}finally{
			System.out.println("最後まで実行しました。");
		}
	}
}


